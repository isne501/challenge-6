#include <iostream>
#include "fraction.h" //include fraction header
using namespace std;

int main()
{
	fraction A; //for stored Numerator A , Denominator A
	fraction B;//for stored Numerator B , Denominator B
	fraction result;//for stored Numerator , Denominator after calculate

	int NumA, NumB, DenA, DenB; //store Numerator A , Denominator A , Numerator B , Denominator B respective

	cout << "Please input your Fraction! (i.e. 2 2 3 4 (mean 2/2 and 3/4) :  "; //Show message promt user input two fraction
	cin >> NumA >> DenA >> NumB >> DenB;

	cout << endl;

	while (DenA == 0) //if Denominator A eqaul to 0 let user input again
	{
		cout << "Denominator of your First fraction mustn't equal to 0 ,Try again : ";
		cin >> DenA;
	}

	while (DenB == 0) //if Denominator B eqaul to 0 let user input again
	{
		cout << "Denominator of your Second fraction mustn't equal to 0 ,Try again : ";
		cin >> DenB;
	}

	A.setNum(NumA); //set Numerator A
	A.setDen(DenA); //set Denominator A
	B.setNum(NumB); //set Numerator B
	B.setDen(DenB); //set Denominator B

	cout << endl;

	cout << "Your fraction are " << A.getNum() << "/" << A.getDen() << " and " << B.getNum() << "/" << B.getDen() << endl <<endl; //Show both fraction

	result = A + B; //Making fraction A + B
	cout << "Sum result : " << result.getNum() << "/" << result.getDen() << endl; //Show result after +

	result = A - B;  //Making fraction A - B
	cout << "Minus result : " << result.getNum() << "/" << result.getDen() << endl; //Show result after -

	result = A * B;  //Making fraction A * B
	cout << "Multiplied result : " << result.getNum() << "/" << result.getDen() << endl; //Show result after *

	result = A / B;  //Making fraction A / B
	cout << "Division result : " << result.getNum() << "/" << result.getDen() << endl; //Show result after /

	cout << endl;

	++A; //Making fraction ++A 
	cout << "++A result : " << A.getNum() << "/" << A.getDen() << endl; //Show value of fraction A now

	A++; //Making fraction A++
	cout << "A++ result : " << A.getNum() << "/" << A.getDen() << endl; //Show value of fraction A now

	++B; //Making fraction ++B
	cout << "++B result : " << B.getNum() << "/" << B.getDen() << endl; //Show value of fraction B now

	B++; //Making fraction B++
	cout << "B++ result : " << B.getNum() << "/" << B.getDen() << endl; //Show value of fraction B now

	--A; //Making fraction --A
	cout << "--A result : " << A.getNum() << "/" << A.getDen() << endl; //Show value of fraction A now

	A--; //Making fraction A--
	cout << "A-- result : " << A.getNum() << "/" << A.getDen() << endl; //Show value of fraction A now

	--B; //Making fraction --B
	cout << "--B result : " << B.getNum() << "/" << B.getDen() << endl; //Show value of fraction B now

	B--; //Making fraction B--
	cout << "B-- result : " << B.getNum() << "/" << B.getDen() << endl; //Show value of fraction B now

	cout << endl;

	if (A != B) //if A not equal to B show this message 
		cout << "Your both fraction aren't equal" << endl;

	if (A >= B || A <= B) 
	{
		if (A > B) //if A more than B show this message 
			cout << "Your fraction " << A.getNum() << "/" << A.getDen() << " are more than " << B.getNum() << "/" << B.getDen() << endl;
		if (A < B) //if A less than B show this message 
			cout << "Your fraction " << A.getNum() << "/" << A.getDen() << " are less than " << B.getNum() << "/" << B.getDen() << endl;
		if (A == B) //if A equal to B show this message 
			cout << "Your both fraction are equal" << endl;
	}

	cout << endl;

	system("PAUSE"); //Pause the system
	return 0;
}
