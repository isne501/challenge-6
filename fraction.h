#ifndef FRACTION_H
#define FRACTION_H

class fraction //name of type
{
public :
	fraction(); //Constructors that default value
	fraction(int num);
	fraction(int num, int den);
	void setNum(int &num); //Mutators update value of numerator
	void setDen(int &den); //update value of denominator
	int getNum(); //Accessors for show numerator
	int getDen(); //for show denominator
	fraction & operator= (const fraction &numOne); // for set "this" store numeratorA and denominatorB
	fraction operator+ (const fraction &numTwo); //Make A+B
	fraction operator- (const fraction &numTwo); //Make A-B
	fraction operator* (const fraction &numTwo); //Make A*B
	fraction operator/ (const fraction &numTwo); //Make A/B
	fraction & operator++ (); //Make ++A or ++B
	fraction & operator++ (int number); //Make A++ or B++
	fraction & operator-- (); //Make --A or --B
	fraction & operator-- (int number); //Make A-- or B--
	bool operator== (fraction &numTwo); //Check, Are fractions equal ?
	bool operator!= (fraction &numTwo); //Check, Are fractions not equal?
	bool operator>= (fraction &numTwo); //Check ,Are fractions A more than B or equal ?
	bool operator<= (fraction &numTwo); //Check ,Are fractions A less than B or equal ?
	bool operator> (fraction &numTwo); //Check ,Are fractions A more than B ?
	bool operator< (fraction &numTwo); //Check ,Are fraction A less than B?

private :
	int numerator; //Numerator
	int denominator; //Denominator
};

#endif
