#include "fraction.h"

fraction::fraction() //Default value of numerator , denominator
{
	numerator = 0;
	denominator = 1;
}

int fraction::getNum() //Show value of numerator
{
	return numerator;
}

int fraction::getDen() //Show value of denominator
{
	return denominator;
}

void fraction::setNum(int &num) //Set value of numerator
{
	numerator = num;
}

void fraction::setDen(int &den) //Set value of denominator
{
	denominator = den;
}

fraction::fraction(int num) //if fraction has only numerator make value of denominator equal to 1 
{
	numerator = num;
	denominator = 1; 
}

fraction::fraction(int num, int den) //if fraction have numerator and denominator 
{
	numerator = num;
	denominator = den;
}

fraction & fraction::operator= (const fraction &numOne) //Need &(passing reference) to update value fraction  
{
	numerator = numOne.numerator; 
	denominator = numOne.denominator; 
	return *this; //"this" stored value of numerator , denominator
}

bool fraction::operator== (fraction &numTwo) //if numeratorA*denominatorB = numeratorB*denominatorA that mean both value are equal
{
	if ((getNum() * numTwo.getDen()) == (getDen() * numTwo.getNum()))
		return true;
	else
		return false;
}

bool fraction::operator!= (fraction &numTwo) //if numeratorA*denominatorB != numeratorB*denominatorA that mean both value aren't equal
{
	if ((getNum() * numTwo.getDen()) != (getDen() * numTwo.getNum()))
		return true;
	else
		return false;
}

bool fraction::operator>= (fraction &numTwo) //if numeratorA*denominatorB >= numeratorB*denominatorA that mean value A > B or A=B
{
	if ((getNum() * numTwo.getDen()) >= (getDen() * numTwo.getNum()))
		return true;
	else
		return false;
}

bool fraction::operator<= (fraction &numTwo) //if numeratorA*denominatorB <= numeratorB*denominatorA that mean value A < B or A=B
{
	if ((getNum() * numTwo.getDen()) <= (getDen() * numTwo.getNum()))
		return true;
	else
		return false;
}

bool fraction::operator> (fraction &numTwo) //if numeratorA*denominatorB > numeratorB*denominatorA that mean value A > B
{
	if ((getNum() * numTwo.getDen()) > (getDen() * numTwo.getNum()))
		return true;
	else
		return false;
}

bool fraction::operator< (fraction &numTwo) //if numeratorA*denominatorB < numeratorB*denominatorA that mean value A < B
{
	if ((getNum() * numTwo.getDen()) < (getDen() * numTwo.getNum()))
		return true;
	else
		return false;
}

 fraction fraction::operator+ (const fraction &numTwo)
{
	fraction result; //Create fraction name result to store numerator and denominator after calculate 
	result.numerator = this->numerator * numTwo.denominator + this->denominator * numTwo.numerator; //Numerator of result = numeratorA*denominatorB + denominatorA*numeratorB
	result.denominator = this->denominator * numTwo.denominator; //Denominator of result = denominatorA * denominatorB
	return result;
}

 fraction fraction::operator- (const fraction &numTwo) //Same as operator+ but change + to -
{
	fraction result;
	result.numerator = this->numerator * numTwo.denominator - this->denominator * numTwo.numerator;
	result.denominator = this->denominator * numTwo.denominator;
	return result;
}


 fraction fraction::operator* (const fraction &numTwo) 
{
	fraction result; //Create fraction name result to store numerator and denominator after calculate
	result.numerator = this->numerator * numTwo.numerator; //Numerator of result = numeratorA*numeratorB
	result.denominator = this->denominator * numTwo.denominator; //Denominator of result = denominatorA*denominatorB
	return result;
}

 fraction fraction::operator/ (const fraction &numTwo)
{
	fraction result;//Create fraction name result to store numerator and denominator after calculate
	result.numerator = this->numerator * numTwo.denominator; //Numerator of result = numeratorA*denominatorB sush as 1/2 / 3/4 means 1/2 * 4/3
	result.denominator = this->denominator * numTwo.numerator; //Denominator of result = denominatorA*numeratorB 
	return result;
}

fraction & fraction::operator++ () //pre increment that mean ++ before variable such as ++number
{
	numerator += denominator; //because "this" stored numerator and denominator if +1 before "this" that means 1+(numerator/denominator) now "this" is update
	return *this;
}

fraction & fraction::operator++ (int number)  //post increment and pass int number for begin with number and following by ++ such as number++
{
	numerator += denominator; //because "this" stored numerator and denominator if +1 after "this" that means (numerator/denominator)+1 now "this" is update
	return *this ;
}

fraction & fraction::operator-- () //pre decrement hat mean -- before variable such as --number
{
	numerator -= denominator; //because "this" stored numerator and denominator if -1 before "this" that means -1+(numerator/denominator) now "this" is update
	return *this;
}

fraction & fraction::operator-- (int number) //post decrement and pass int number for begin with number and following by -- such as number--
{
	numerator -= denominator; //because "this" stored numerator and denominator if -1 after "this" that means (numerator/denominator)-1 now "this" is update
	return *this;
}



